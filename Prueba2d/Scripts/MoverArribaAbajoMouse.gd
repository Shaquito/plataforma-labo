extends StaticBody2D
var puede_modificar = false
@export var limiteMax = 0
@export var limiteMin = 0
@onready var posicionInicial = position.y

func detectar_scroll():#Si detecta que el cursor esta sobre el objeto a modificar y esta presionando la tecla, escala
	if puede_modificar and position.y > limiteMax and Input.is_action_pressed("flecha_der"):
		position.y -= 2 
		#print(position.y)
	elif puede_modificar and position.y < limiteMin and Input.is_action_pressed("flecha_izq"):
		position.y += 2
		#print(position.y)
	
func _physics_process(delta):
	detectar_scroll()
	pass

func devolverPosicion():#CAMBIAR A QUE VUELVA A POSICION INICIAL
	if position.y < posicionInicial:
		#print(position.y)
		position.y += 1.5
	elif position.y > posicionInicial:
		#print(position.y)
		position.y -= 1.5

func ComenzarAdevolverPosicion():#Startea el timer de ticks para escalar en ese tiempo
	$Timer2.start()
	pass

func _on_mouse_entered():
	puede_modificar = true
	$Timer.stop()
	$Timer2.stop()
	pass # Replace with function body.

func _on_mouse_exited():
	puede_modificar = false
	#print("Ha comenzado el timer")
	$Timer.start()
	pass # Replace with function body.


func _on_timer_timeout():
	ComenzarAdevolverPosicion()
	pass # Replace with function body.


func _on_timer_2_timeout():
	devolverPosicion()
	pass # Replace with function body.
