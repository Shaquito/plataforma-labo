extends CharacterBody2D
var velocidad = 250
var fuerza_salto = 700
var direccion = Input.get_axis("izquierda","derecha")
var esta_atacando = false
@onready var ap = $AnimationPlayer
@onready var sprite = $Sprite2D
@onready var camera = $Camera2D

func comprobarGiro():
	if direccion < 0:
		sprite.flip_h = true
		#mueve la hitbox del ataque
		$Area2D/CollisionShape2D.position.x = -20
	elif direccion > 0:
		sprite.flip_h = false
		#mueve la hitbox del ataque
		$Area2D/CollisionShape2D.position.x = 24.5

func comprobarSalto():
	if is_on_floor():
		if Input.is_action_just_pressed("saltar"):
			velocity.y -= fuerza_salto

func comprobarMovimiento():
	
	if Input.is_action_pressed("correr"):
		velocidad = 450
	else:
		velocidad = 250
	direccion = Input.get_axis("izquierda","derecha")
	#Mover personaje
	velocity.x = direccion * velocidad
	#para caer mas rapido presionando S
	if Input.is_action_pressed("abajo"):
		Autoloads.GRAVEDAD * 2
	#else:
		#Autoloads.GRAVEDAD

func comprobarAnimacion():
	
	#si se mueve y esta en el piso RUN
	if velocity.x != 0 and is_on_floor():
		ap.play("run")
	#comprueba hacia donde tiene que flipear el sprite
	comprobarGiro()
	#Si esta quieto y esta en el piso IDLE
	if is_on_floor and velocity.x == 0:
		ap.play("idle")
		
		#Si no esta en el piso
	if !is_on_floor():
		if velocity.y < 0:#esta ascendiendo
			ap.play("jump")
		else: #esta bajando
			ap.play("fall")

func _physics_process(delta):
	comprobarMovimiento()
	comprobarSalto()
	if !esta_atacando:
		comprobarAnimacion()
		if Input.is_action_just_pressed("atacar"):
			$Timer.wait_time = 0.3
			$Timer.start()
			esta_atacando = true
			ap.play("attack1")
	velocity.y += Autoloads.GRAVEDAD
	
	
	
	
	move_and_slide()


func _on_timer_timeout():
	esta_atacando = false
	pass # Replace with function body.

func _on_area_2d_body_entered(body):
	if body.has_method("take_damage"):
		body.take_damage()
	pass # Replace with function body.
