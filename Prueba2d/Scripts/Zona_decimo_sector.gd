extends Area2D
@onready var player = get_parent().get_parent().get_node("Player/")
@onready var player_camera = get_parent().get_parent().get_node("Player/Camera2D")
@onready var zona_parkour = get_parent().get_parent().get_node("Nivel_2/Parkour_vertical")
var ya_entro = false

func _on_body_entered(body):
	if body == player:
		if !ya_entro:
			ya_entro = true
			zona_parkour.bajar = true
			print("Player entro")
		player_camera.zoom = Vector2(1.5,1.5)
		pass


func _on_body_exited(body):
	if body == player:
		print("Player salio")
		player_camera.zoom = Vector2(2,2)
	pass # Replace with function body.
