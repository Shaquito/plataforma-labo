extends CharacterBody2D

var gravedad = 30
var velocidad = 50
var recibe_daño = false
var esta_muerto = false
var health = 3
var esta_atacando = false
var direccion_antes_ataque

@onready var player = get_parent().get_parent().get_parent().get_node("Player")
@onready var izq_o_der = randi_range(0,1)

func take_damage():
	recibe_daño = true
	health -=1
	$Timer.wait_time = 0.21 #duracion del ataque1
	$Timer.start()
	$AnimationPlayer.play("take_hit")
	if health <= 0:
		esta_muerto = true
		$AnimationPlayer.play("death")
		$Timer.wait_time = 0.35
		$Timer.start()


func _on_timer_timeout():
	recibe_daño = false
	if esta_muerto:
		queue_free()
	pass # Replace with function body.
func atacar():
	$Tiempo_de_ataque.wait_time = 2
	$Tiempo_de_ataque.start()
	esta_atacando = true
	$AnimationPlayer.play("attack")
	print("Esta atacando")
	
	
func atacar_player():
	if $Ray_L.is_colliding:
		if $Ray_L.get_collider()== player and !esta_atacando: #Comprueba si esta viendo al player y de se posible lo ataca
			direccion_antes_ataque = velocity.x
			velocity.x = 0
			$Sprite2D.flip_h = true
			$Area_ataque/CollisionShape2D.position = Vector2(-66,2)
			print(player)
			atacar()

	if $Ray_R.is_colliding:
		if $Ray_R.get_collider()== player and !esta_atacando:
			direccion_antes_ataque = velocity.x
			velocity.x = 0
			$Sprite2D.flip_h = false
			$Area_ataque/CollisionShape2D.position = Vector2(16,2)
			atacar()

func comprobar_direccion_sprites():
	if velocity.x < 0:
		$Sprite2D.flip_h = true
	elif velocity.x > 0:
		$Sprite2D.flip_h = false

func comprobar_movimiento():
	comprobar_direccion_sprites()
# Determina si se puede mover hacia izquierda o derecha
	if $Ray_AD.get_collider() == null or $Ray_R2.get_collider() != null:
		velocity.x = -velocity.x
	if $Ray_AI.get_collider() == null or $Ray_L2.get_collider() != null:
		velocity.x = -velocity.x



func patrullar():
	comprobar_movimiento()
	pass

func iniciar_movimiento():
	if izq_o_der == 0:
		velocity.x = velocidad
	else:
		velocity.x = -velocidad

func _ready():
	iniciar_movimiento()

func _physics_process(delta):
	if !recibe_daño and !esta_atacando:
		patrullar()
		$AnimationPlayer.play("idle")
	atacar_player()
	velocity.y += gravedad
	move_and_slide()

func _on_tiempo_de_ataque_timeout():
	esta_atacando = false
	velocity.x = direccion_antes_ataque
