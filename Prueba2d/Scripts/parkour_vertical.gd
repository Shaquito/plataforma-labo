extends StaticBody2D
var puede_modificar = false
@export var limiteMax = 2.4
@export var limiteMin = 0.5
var escalaInicial = scale

func detectar_scroll():#Si detecta que el cursor esta sobre el objeto a modificar y esta presionando la tecla, escala
	if puede_modificar and scale.x > limiteMin and Input.is_action_pressed("flecha_izq"):
		scale -= Vector2(0.003,0.003)
	elif puede_modificar and scale.x < limiteMax and Input.is_action_pressed("flecha_der"):
		scale += Vector2(0.003,0.003)
	
func _physics_process(delta):
	detectar_scroll()
	pass
	

func _on_mouse_entered():
	print("Hola")
	puede_modificar = true
	pass # Replace with function body.


func _on_mouse_exited():
	$Timer.start()
	print("Chau")
	puede_modificar = false
	pass # Replace with function body.

#func devolverEscala():#Si tiene que agrandar agranda sino achica
#	if scale < escalaInicial:
#		scale += Vector2(0.01,0.01)
#	elif scale > escalaInicial:
#		scale -= Vector2(0.01,0.01)

#func ComenzarAdevolverEscala():#Startea el timer de ticks para escalar en ese tiempo
#	$Timer2.start()
#	pass

#func _on_timer_2_timeout():
#	devolverEscala()
#	pass # Replace with function body.


#func _on_timer_timeout():
#	ComenzarAdevolverEscala()
#	pass # Replace with function body.
