extends Node2D

@onready var posicionInicial = position.y
@onready var limiteMaximo = -20
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_input():
	if Input.is_action_pressed("flecha_aba") and global_position.y < limiteMaximo:
		position.y += 10
		$Timer.start()
		
	pass

func _physics_process(delta):
	get_input()
	pass

func devolverPosicion():
	if position.y != posicionInicial:
		position.y = posicionInicial
		pass

func _on_timer_timeout():
	devolverPosicion()
	pass # Replace with function body.
