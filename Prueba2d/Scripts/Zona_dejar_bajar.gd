extends Area2D
@onready var player = get_parent().get_parent().get_node("Player/")
@onready var player_camera = get_parent().get_parent().get_node("Player/Camera2D")
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_body_entered(body):
	if body == player:
		print("Player entro")
		player_camera.zoom = Vector2(1.5,1.5)
		pass


func _on_body_exited(body):
	if body == player:
		print("Player salio")
		player_camera.zoom = Vector2(2,2)
	pass # Replace with function body.
