extends StaticBody2D

var abajo = false

func _ready():
	$Timer3.start()
	
func _physics_process(delta):
	if abajo == true:
		position.y += 1
	else:
		position.y -= 1

func _on_timer_3_timeout():
	if abajo == true:
		abajo = false
	else:
		abajo = true
	pass # Replace with function body.

