extends StaticBody2D

var abajo = true

func _ready():
	$Timer.start()
	var rand = randi_range(0,1)
	if rand == 0:
		abajo = true
	else:
		abajo = false
func _physics_process(delta):
	if abajo == true:
		position.y += 0.5
	else:
		position.y -= 0.5


func _on_timer_timeout():
	if abajo == true:
		abajo = false
	else:
		abajo = true
	pass # Replace with function body.
